package com.example.testdemo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
public class TestController {
    @RequestMapping("test")
    public String testSpringBoot() {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
        String time_str = sdf.format(d);
        System.out.println("输出：" + time_str);
        return " V2.0.0 hello " + time_str;
    }
}